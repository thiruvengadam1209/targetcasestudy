MyRetail Target Case study Solution provides the ability to:

    Retrieve product information using ProductId, from a REST api
    
    Retrieve price information using Product Id, from a NoSQL Data store 
    
    Update the price information in the database.

Technology Stack:

Spring Boot :
https://start.spring.io/

MongoDB: 
https://docs.mongodb.com/manual/tutorial/getting-started/

Maven: 
https://maven.apache.org/

Postman: 

Installation required :

Install Mongo DB:
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/
  Assuming, Java, maven would have been already present
 
Clone the git project from git-bash or command prompt (You must have git setup)

Import the project into eclipse/IntelliJ � File->import from existing sources

Main method present in targetcasestudy/src/main/groovy/com/target/Retail.groovy