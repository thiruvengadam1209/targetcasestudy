package com.target.service

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.apache.http.HttpEntity
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
@Slf4j
class ProductServiceClient {

    @Value('${scheme}')
    String scheme

    @Value('${redSkyHost}')
    String redSkyHost

    @Value('${redSkyPath}')
    String redSkyPath

    @Value('${exclusionParameters}')
    String exclusionParameters

    String EXCLUDE_PARAMETER="excludes"

    String getProductDetails(String productId){
        URI uri = getUri(productId)
        log.info("Resource to be invoked to get product details --> {}", uri.toString())
        HttpGet httpGet = new HttpGet(uri.toString())
        String responseString
        try{
            CloseableHttpClient httpClient = HttpClients.createDefault()
            CloseableHttpResponse response = httpClient.execute(httpGet)
            HttpEntity entity = response.getEntity()
            responseString = EntityUtils.toString(entity, "UTF-8")
            log.info("responseString---> {}",responseString)
            def productDetailsResponse = new ObjectMapper().readValue(responseString, Map.class)
            String productName = productDetailsResponse?.'product'?.'item'?.'product_description'?.'title' ?: "TARGET_PRODUCT_" + productId
            productName
        } catch(Exception exception){
            log.error("Product Details not present / Issue in call")
            return null
        }


    }

     URI getUri(String productId){
        new URIBuilder().setScheme(scheme)
                        .setHost(redSkyHost)
                        .setPath(redSkyPath+productId)
                        .setParameter(EXCLUDE_PARAMETER,exclusionParameters)
                        .build()
    }

}
