package com.target.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.target.Exception.DatastoreNotFoundException
import com.target.Exception.InvalidProductException
import com.target.Exception.ProductIdMismatchException
import com.target.Exception.ProductNotFoundException
import com.target.domain.AggregatedProductInfo
import com.target.domain.PriceInfo
import com.target.domain.ProductPriceInfo
import com.target.repository.ProductPriceRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
@Slf4j
class ProductDetailsAggregatorService {

    @Autowired
    ProductPriceRepository productPriceRepository

    @Autowired
    ProductServiceClient productServiceClient

    def getAggregatedProductInfo(String id){
        AggregatedProductInfo aggregatedProductInfo = new AggregatedProductInfo()
        aggregatedProductInfo.setId(id)
        retrievePriceDetailsFromDataStore(id, aggregatedProductInfo)
        retrieveProductDetailsFromEnterprise(id, aggregatedProductInfo)
        log.info(aggregatedProductInfo.getName() + " :: " +new ObjectMapper().writeValueAsString(aggregatedProductInfo))
        aggregatedProductInfo
    }

    private AggregatedProductInfo retrievePriceDetailsFromDataStore(String id, AggregatedProductInfo aggregatedProductInfo) {
        ProductPriceInfo productPriceInfo
        try{
            if(!id)
                throw new InvalidProductException()
            Integer.parseInt(id)
            productPriceInfo = productPriceRepository.findByProductId(id) ?: null
        }catch(NumberFormatException){
            throw new InvalidProductException()
        }catch(Exception exception){
            throw new DatastoreNotFoundException()
        }
        if(!productPriceInfo)
            throw new ProductNotFoundException()
        log.info("ProductId is {}, current_Price is {}", productPriceInfo.getProperty("productId"), productPriceInfo.getProperty("currentPrice").getAt("value"))
        PriceInfo priceInfo = new PriceInfo(productPriceInfo.getProperty("currentPrice").getAt("value"), productPriceInfo.getProperty("currentPrice").getAt("currencyCode"))
        aggregatedProductInfo.setCurrent_price(priceInfo)
        aggregatedProductInfo
    }

    private AggregatedProductInfo retrieveProductDetailsFromEnterprise(String id, AggregatedProductInfo aggregatedProductInfo) {
        String productName = productServiceClient.getProductDetails(id)
        log.info("ProductName --->>" + productName)
        if(productName)
            aggregatedProductInfo.setName(productName)
        else
            throw new ProductNotFoundException()
        aggregatedProductInfo
    }

    def updateProduct(String productId, AggregatedProductInfo aggregatedProductInfo){
      log.info("Input price is ---> {}", aggregatedProductInfo.getProperty("current_price").getAt("value"))
        try{
            //Validations
            if(!productId)
                throw new InvalidProductException()
            Integer.parseInt(productId)
            if(!productId.equals(aggregatedProductInfo.getId()))
                throw new ProductIdMismatchException()

            productPriceRepository.save(new ProductPriceInfo(productId,new PriceInfo(aggregatedProductInfo.getProperty("current_price").getAt("value"),"USD")))
            ProductPriceInfo productPriceInfo = productPriceRepository.findByProductId(productId) ?: null

           if(null == productPriceInfo || (productPriceInfo.getProperty("currentPrice").getAt("value") != aggregatedProductInfo.getProperty("current_price").getAt("value")))
               throw new DatastoreNotFoundException()

        }catch(NumberFormatException nfe){
            throw new InvalidProductException()
        }catch(ProductIdMismatchException pme){
            throw new ProductIdMismatchException()
        }catch(Exception e){
            throw new DatastoreNotFoundException()
        }
        aggregatedProductInfo
    }
}
