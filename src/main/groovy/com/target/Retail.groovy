package com.target

import com.target.domain.PriceInfo
import com.target.domain.ProductPriceInfo
import com.target.repository.ProductPriceRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
@Slf4j
class Retail extends SpringBootServletInitializer implements CommandLineRunner {

    @Autowired
    private ProductPriceRepository productPriceRepository

    @Value('${productIdList}')
    String productIdList

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
        application
    }


     @Override
     public void run(String... args) throws Exception {
        productPriceRepository.deleteAll()
         // load sample data
         def randomPrice = 3.49
         def productIdList = productIdList.split(',')
         productIdList.toList()?.each {
             randomPrice+=10
             writeToProductPriceRepository(it, String.valueOf(randomPrice))
         }
        // fetch loaded data
        log.info("Products found with findAll():")
        log.info("-------------------------------")
        productPriceRepository?.findAll().each {log.info(it.getProperty("productId")+ " :: "+ it.getProperty("currentPrice").getAt("value"))}
     }

        void writeToProductPriceRepository(String productId, String priceOfTheProduct){
            productPriceRepository.save(new ProductPriceInfo(productId,new PriceInfo(priceOfTheProduct,"USD")))
        }

    public static void main(String[] args){
        SpringApplication.run(Retail, args)
    }
}


