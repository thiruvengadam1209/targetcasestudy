package com.target.domain

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize


@JsonPropertyOrder(["id","name","current_price"])
class AggregatedProductInfo implements Serializable {
    @JsonProperty("id")
    String id
    @JsonProperty("name")
    String name
    @JsonProperty("current_price")
    PriceInfo current_price

    AggregatedProductInfo(){

    }
}
