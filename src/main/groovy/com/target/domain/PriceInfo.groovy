package com.target.domain

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class PriceInfo implements Serializable {

    @JsonProperty("value")
    String value
    @JsonProperty("currency_code")
    String currencyCode


    PriceInfo(String value, String currencyCode){
        this.value = value
        this.currencyCode = currencyCode
    }

    PriceInfo(){

    }
}
