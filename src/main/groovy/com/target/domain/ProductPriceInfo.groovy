package com.target.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document(collection='products')
class ProductPriceInfo implements Serializable{
        @Id
        String productId
        PriceInfo currentPrice

        ProductPriceInfo(String productId, PriceInfo currentPrice){
           this.productId = productId
           this.currentPrice = currentPrice
        }

    ProductPriceInfo(){

    }
}
