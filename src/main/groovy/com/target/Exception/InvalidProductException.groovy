package com.target.Exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "This product Id seems to be invalid. Please try with valid product Id")
class InvalidProductException extends RuntimeException {

    public InvalidProductException(){

    }
}
