package com.target.Exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Problem with our internal connections. Please Retry after sometime")
class DatastoreNotFoundException extends RuntimeException{

    public DatastoreNotFoundException(){

    }
}
