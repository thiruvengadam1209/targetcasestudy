package com.target.Exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The Product Id in the resource is not matching with the request body")
class ProductIdMismatchException extends RuntimeException {

    public ProductIdMismatchException(){

    }
}
