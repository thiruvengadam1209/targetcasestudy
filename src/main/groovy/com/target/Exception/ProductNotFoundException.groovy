package com.target.Exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Product not found.")
class ProductNotFoundException extends RuntimeException{

    public ProductNotFoundException(){

    }
}
