package com.target.repository

import com.target.domain.AggregatedProductInfo
import com.target.domain.ProductPriceInfo
import org.springframework.data.mongodb.repository.MongoRepository


interface ProductPriceRepository extends MongoRepository<ProductPriceInfo,String> {

    public ProductPriceInfo findByProductId(String productId)

}
