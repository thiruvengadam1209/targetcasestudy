package com.target.controller

import com.target.repository.ProductPriceRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

import java.nio.charset.Charset

@Controller
@Slf4j
class HeartBeatController {

    @Value('${application.name}')
    String applicationName

    @Value('${application.version}')
    String applicationVersion

    @GetMapping(value="/heartbeat")
    ResponseEntity getHeartbeat() {
        HttpHeaders headers = new HttpHeaders()
        headers.setContentType(new MediaType("application", "json", Charset.forName('UTF-8')))
        //String response = ProductPriceRepo
        return new ResponseEntity<String>("""{"applicationName":"${applicationName.value}","applicationVersion":"${applicationVersion.value}"}""" as String, headers, HttpStatus.OK)
    }
}
