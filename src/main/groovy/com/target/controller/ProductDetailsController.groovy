package com.target.controller

import com.target.domain.AggregatedProductInfo
import com.target.service.ProductDetailsAggregatorService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@Slf4j
class ProductDetailsController {

    @Autowired
    ProductDetailsAggregatorService productDetailsService

    @GetMapping(value="/products/{productId}", produces = "application/json")
    @ResponseBody
    Object getProductDetails(@PathVariable String productId){
        log.info("Retrieving aggregated product info for ${productId}")
        productDetailsService.getAggregatedProductInfo(productId)
    }

    @PutMapping(value="/products/{productId}")
    @ResponseBody
    Object updateProductDetails(@PathVariable String productId, @RequestBody AggregatedProductInfo productInfo){
        log.info("Retrieving aggregated product info for ${productId}")
        productDetailsService.updateProduct(productId, productInfo)
    }
}
